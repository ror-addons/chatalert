<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

	<UiMod name="ChatAlert" version="1.9.6" date="10/01/2008" >
	
		<Author name="uce_mike" email="uce_mike@yahoo.com" />
	
		<Description text="Sound alerts on incoming messages." />
	
<VersionSettings gameVersion="1.4.5" windowsVersion="1.0" savedVariablesVersion="1.0" />
        <Dependencies>
            <Dependency name="LibSlash" />
        </Dependencies>

		<Files>
			<File name="ChatAlert.lua" />
		</Files>
		
		<SavedVariables>
		  <SavedVariable name="ChatAlertFilterOptions" />
		</SavedVariables>

		<OnInitialize>
			<CallFunction name="ChatAlert.Initialize" />
		</OnInitialize>

		<OnShutdown>
			<CallFunction name="ChatAlert.Shutdown" />
		</OnShutdown>


	</UiMod>

</ModuleFile>    