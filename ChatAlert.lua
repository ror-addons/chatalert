--[[

    Simple mod to play sounds when certain text events sent to player. Got tired of missing tells
    and /gu.
    
    switch to.
    Sound.Play( Sound.APOTHECARY_ITEM_REMOVED )

--]]
ChatAlert = {}
CA_Version = "1.9.6"

local function print(txt)
    TextLogAddEntry("Chat", 0, towstring(txt))
end

function ChatAlert.Initialize()
    if not ChatAlertFilterOptions then
        ChatAlertFilterOptions = {
            Channels = {
            ALLIANCE            = { name = "ALLIANCE",          isDisabled = false, soundID = 215, type = 12,},
            ALLIANCE_OFFICER    = { name = "ALLIANCE_OFFICER",  isDisabled = false, soundID = 1108,type = 13,},
            CITY_ANNOUNCE       = { name = "CITY_ANNOUNCE",     isDisabled = false, soundID = 217, type = 21,},
            GROUP               = { name = "GROUP",             isDisabled = false, soundID = 219, type = 4,},
            GUILD               = { name = "GUILD",             isDisabled = false, soundID = 230, type = 8,},
            GUILD_OFFICER       = { name = "GUILD_OFFICER",     isDisabled = false, soundID = 218, type = 9,},
            RVR                 = { name = "RVR",               isDisabled = false, soundID = 1109,type = 17,},
            SCENARIO            = { name = "SCENARIO",          isDisabled = false, soundID = 219, type = 15,},
            SCENARIO_GROUPS     = { name = "SCENARIO_GROUPS",   isDisabled = false, soundID = 212, type = 16,},
            SHOUT               = { name = "SHOUT",             isDisabled = false, soundID = 1107,type = 11,},
            TELL_SENT           = { name = "TELL_SENT",         isDisabled = false, soundID = 216, type = 2,},
            TELL_RECEIVE        = { name = "TELL_RECEIVE",      isDisabled = false, soundID = 214, type = 1,},
            BATTLEGROUP         = { name = "BATTLEGROUP",       isDisabled = false, soundID = 1111,type = 14,},
            },
-- debug var
            useDebug            = false,
        }
    end

 	RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "ChatAlert.IncomingChat");
    if not LibSlash.IsSlashCmdRegistered("/ca") then
        LibSlash.RegisterSlashCmd("ca", function(args) ChatAlert.SlashHandler(args) end)
    else
        print("Warning: something else seems to be using /ca - ChatAlert won't be able to. Use /chatalert instead.")
    end
    LibSlash.RegisterSlashCmd("chatalert", function(args) ChatAlert.SlashHandler(args) end)
 	
	TextLogAddEntry("Chat", 0, L"Chat Alert! loaded");
end

function ChatAlert.SlashHandler(args)
    local opt, val = args:match("([a-z0-9]+)[ ]?(.*)")
    
    -- NO OPTION SPECIFIED
    if not opt then
        TextLogAddEntry("Chat", 0, L"------------Chat Alert! "..towstring(CA_Version)..L"-----------------")
        print("Valid options for /chatalert:")
        print("toggle [CHANNEL] - enables/disables alerts for CHANNEL.")
        print("sound [CHANNEL] [soundID] - set sound played for CHANNEL to soundID.")
        print("play [soundID] - test soundID sound.")
        print("slist - list sound IDs Mythic has avaliable.")
        print("channels - lists all the channels you can toggle alerts for.")
        
     -- toggle debug
    elseif opt == "debug" then
        ChatAlertFilterOptions.useDebug = not ChatAlertFilterOptions.useDebug
        if ChatAlertFilterOptions.useDebug then
            print("Chat Alert debug enabled.")
        else
            print("Chat Alert debug disabled.")
        end   -- list channels
     -- sound ID set
    elseif opt == "sound" then
        local bFound = false
        local sChannel, sID = val:match("([a-z0-9_]+)[ ]?(.*)")
        for i, id in pairs(ChatAlertFilterOptions.Channels) do
            if ((sChannel:upper()) == id.name) then
                TextLogAddEntry("Chat", 0, L"-"..towstring(sChannel)..L" sound id changed from "..towstring(id.soundID)..L" to "..towstring(sID));            
                id.soundID = tonumber(sID)
                bFound = true
                break
            end
        end
        if not bFound then
            TextLogAddEntry("Chat", 0, L"Unable to set sound for channel "..towstring(sChannel)..L" to "..towstring(sID));            
        end

    -- list channels
    elseif opt == "channels" then
        print("--------Channels----------")
        for i, id in pairs(ChatAlertFilterOptions.Channels) do
            TextLogAddEntry("Chat", 0, L""..towstring(id.name)..L", sound id "..towstring(id.soundID));            
        end
    -- slist sound ids
    elseif opt == "slist" then
        local mySounds = {}
        print("--------Sound IDs----------")
        for i, id in pairs(Sound) do
            mySounds.name = towstring(i)
            mySounds.soundID = towstring(id)
            TextLogAddEntry("Chat", 0, L""..towstring(mySounds.soundID)..L"="..towstring(mySounds.name));
            --TextLogAddEntry("Chat", 0, L""..towstring(i)..L"="..towstring(id));
        end
     -- play soundID
    elseif opt == "play" then
        if val ~= nil then
            print("-Playing sound ID "..val)
            PlaySound(tonumber(val))
        end
   -- TOGGLE
    elseif opt == "toggle" then
        if val == "" or not val then
            print("Usage: /chatfilter toggle [CHANNEL]")
        else
            local str = val:upper()
            local bval = false
            local bFound = false

            for i, id in pairs(ChatAlertFilterOptions.Channels) do
            --    print (""..id.name)
                if str == id.name then 
                    id.isDisabled = not id.isDisabled
                    bval = id.isDisabled
                    bFound = true
                end
            end
            
            if not bFound then
                TextLogAddEntry("Chat", 0, L"Invalid channel"..str..".");            
                return
            end
            
            if bval then
                print(str.." will NO LONGER generate a chat alert.");            
            else
                print(str.." will NOW generate a chat alert.");            
            end
        end
        else 
            print ("Invalid Chat Alert option, /chatalert for option list.")
    end
end

-- remove events
function ChatAlert.Shutdown()
 	UnregisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "ChatAlert.IncomingChat");
end

--- play sounds on incoming chat types
function ChatAlert.IncomingChat()
            for i, id in pairs(ChatAlertFilterOptions.Channels) do
                if GameData.ChatData.type == id.type and not id.isDisabled then 
                    if ChatAlertFilterOptions.useDebug then 
                        print("Chat Alert Debug: "..id.name) 
                    end
    
               		PlaySound(id.soundID);
                end
            end

end